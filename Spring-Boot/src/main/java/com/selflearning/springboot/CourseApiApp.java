/**
 * 
 */
package com.selflearning.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author smand6
 *
 */

@SpringBootApplication
public class CourseApiApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(CourseApiApp.class, args);
	}

}
