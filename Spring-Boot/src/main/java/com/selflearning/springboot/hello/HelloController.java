package com.selflearning.springboot.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author smand6
 *
 */
@RestController
public class HelloController {
	/**
	 * @return
	 */
	@RequestMapping("/hello")
	public String sayHi() {
		return "Hi";
	}
}
