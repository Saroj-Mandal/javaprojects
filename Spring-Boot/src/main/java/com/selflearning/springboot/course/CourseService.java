package com.selflearning.springboot.course;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseService {

	@Autowired
	private CourseRepository courseRepository;

	public List<Course> getAllCourses(String topicId) {
		List<Course> Courses = new ArrayList<>();
		courseRepository.findByTopicId(topicId).forEach(Courses::add);
		return Courses;
	}

	public Course getCourse(String id) {
		return courseRepository.findOne(id);
	}

	public Course addCourse(Course Course) {
		courseRepository.save(Course);
		return Course;
	}

	public Course updateCourse(Course Course) {
		courseRepository.save(Course);
		return Course;
	}

	public void deleteCourse(String id) {
		courseRepository.delete(id);
	}
}
