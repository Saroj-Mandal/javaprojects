package com.selflearning.thread;

public class RaceConditionDemo {

	public static void main(String[] args) {
		BankAccount task = new BankAccount();
		task.setBalance(100);

		Thread john = new Thread(task);
		Thread anita = new Thread(task);
		john.setName("John");
		anita.setName("Anita");
		john.start();
		anita.start();
		anita.setPriority(Thread.MAX_PRIORITY);
	}
}

class BankAccount implements Runnable {
	private int balance;

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public void run() {
		System.out.println(Thread.currentThread().getName() + " is starting the app, current balance " + balance);
		makeWithdrawal(75);
		if (balance < 0) {
			System.out.println("Money overdrawn!!!, current balance " + balance);
		}
	}

	private synchronized void makeWithdrawal(int amount) {
		if (balance >= amount) {
			System.out.println(Thread.currentThread().getName() + " is about to withdraw ..., current balance " + balance);
			balance -= amount;
			System.out.println(Thread.currentThread().getName() + " has withdrawn " + amount + " bucks , current balance " + balance);
		} else {
			System.out.println("Sorry, not enough balance for " + Thread.currentThread().getName() + " current balance " + balance);
		}
	}

}