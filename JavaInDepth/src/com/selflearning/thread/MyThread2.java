package com.selflearning.thread;

import java.util.concurrent.TimeUnit;

public class MyThread2 extends Thread {

	public static void main(String[] args) {
		Thread thread = new MyThread2();
		thread.start();
		System.out.println("Inside main() ...");
		try {
			System.out.println("Sleeping thread " + Thread.currentThread());
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < 100; i++) {
			System.out.println("Inside main() ... count = " + i);
		}
	}

	@Override
	public void run() {
		System.out.println("Inside run() ...");
		go();
	}

	private void go() {
		System.out.println("Inside go() ...");
		try {
			System.out.println("Sleeping thread " + Thread.currentThread());
			Thread.sleep(2001);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		more();
	}

	private void more() {
		System.out.println("Inside more() ...");
	}

}