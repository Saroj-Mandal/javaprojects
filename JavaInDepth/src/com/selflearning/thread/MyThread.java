package com.selflearning.thread;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class MyThread {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(scanner.nextLine());
		Task task = new Task();
		Thread thread = new Thread(task);
		thread.start();
		System.out.println("Inside main() ...");
		try {
			System.out.println("Sleeping thread " + Thread.currentThread().getName());
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < 100; i++) {
			System.out.println("Inside main() ... count = " + i);
		}
	}

}

class Task implements Runnable {

	@Override
	public void run() {
		System.out.println("Inside run() ...");
		go();
	}

	private void go() {
		System.out.println("Inside go() ...");
		try {
			System.out.println("Sleeping thread " + Thread.currentThread().getName());
			Thread.sleep(2001);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		more();
	}

	private void more() {
		System.out.println("Inside more() ...");
	}

}