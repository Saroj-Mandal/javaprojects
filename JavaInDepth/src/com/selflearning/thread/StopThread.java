package com.selflearning.thread;

import java.util.concurrent.TimeUnit;

public class StopThread {
	private static volatile boolean stop;

	public static void main(String[] args) throws InterruptedException {
		new Thread(new Runnable() {
			long count = 0;
			public void run() {
				System.out.println("starting thread..");
				while (!stop) {
					System.out.println("In while ..." + count);
					count++;
				}
				count = 0;
				System.out.println("Inner thread ends..");
			}
			
		}).start();

		TimeUnit.MILLISECONDS.sleep(100);
		stop = true;
		for (int i = 0; i < 1000; i++) {
			System.out.println(i);
		}

	}
}
