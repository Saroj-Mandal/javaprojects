package com.selflearning.enums;

public enum BookGenre {
	HORROR(10), BIOGRAPHY;

	public static void main(String[] args) {
		for (BookGenre bookGenre : BookGenre.values()) {
			System.out.println(bookGenre);
			System.out.println(bookGenre.equals(BookGenre.HORROR));
		}
	}

	private int minAgeToRead;

	private BookGenre(int minAgeToRead) {
		this.minAgeToRead = minAgeToRead;
	}

	public int getMinAgeToRead() {
		return minAgeToRead;
	}

	private BookGenre() {
	}
}
