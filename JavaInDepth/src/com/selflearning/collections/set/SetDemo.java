package com.selflearning.collections.set;

import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class SetDemo {

	private static void hashSetDemo() {
		Set<String> set1 = new HashSet<>();
		set1.add("a");
		set1.add("b");
		set1.add("a");

		System.out.println("set1: " + set1);

		Book book1 = new Book("Walden", "Henry Thoreau", 1854);
		Book book2 = new Book("Walden", "Henry Thoreau", 1854);
		Set<Book> set2 = new HashSet<>();
		set2.add(book1);
		set2.add(book2);
		System.out.println("Hashcode of Book1 : " + book1.hashCode());
		System.out.println("Hashcode of Book2 : " + book2.hashCode());
		System.out.println("set2: " + set2);
	}

	public static void linkedHashSetDemo() {
		Set<String> hashSet = new HashSet<>();
		hashSet.add("Raj");
		hashSet.add("John");
		hashSet.add("Anita");
		System.out.println("hashSet: " + hashSet);

		Set<String> linkedHashSet = new LinkedHashSet<>();
		linkedHashSet.add("Raj");
		linkedHashSet.add("John");
		linkedHashSet.add("Anita");
		System.out.println("linkedHashSet: " + linkedHashSet);
	}

	private static void treeSetDemo() {
		Book book1 = new Book("Harry Potter", "J.K.Rowling", 1997);
		Book book2 = new Book("Harry Potter", "J.K.Rowling", 1997);
		Book book3 = new Book("Walden", "Henry David Thoreau", 1854);
		Book book4 = new Book("Effective Java", "Joshua Bloch", 2008);

		Set<Book> books = new TreeSet<>(new TitleComparator());
		books.add(book1);
		books.add(book2);
		books.add(book3);
		books.add(book4);

		for (Book book : books) {
			System.out.println(book);
		}
	}

	private static void navigableSetDemo() {
		NavigableSet<Integer> set = new TreeSet<>();
		set.add(5);
		set.add(23);
		set.add(74);
		set.add(89);
		int var1 = 74;
		System.out.println("set: " + set);
		System.out.println("lower: of " + var1 + " is : " + set.lower(var1));
		System.out.println("floor: of " + var1 + " is : " + set.floor(var1));
		System.out.println("ceiling: of " + var1 + " is : " + set.ceiling(var1));
		System.out.println("higher: of " + var1 + " is : " + set.higher(var1));

		System.out.println("first: " + set.first());
		System.out.println("last: " + set.last());

		System.out.println("set: " + set);

		NavigableSet<Integer> descendingSet = set.descendingSet();
		System.out.println("descendingSet: " + descendingSet);
		int var2 = 74;
		NavigableSet<Integer> inclusiveHeadSet = set.headSet(var2, true);
		System.out.println("headSet: of " + var2 + " inclusive of the element passed is : " + inclusiveHeadSet);

		NavigableSet<Integer> exclusiveHeadSet = set.headSet(var2, false);
		System.out.println("headSet: of " + var2 + " exclusive of the element passed is : " + exclusiveHeadSet);

		inclusiveHeadSet.add(6);
		System.out.println("headSet: " + inclusiveHeadSet);
		System.out.println("set: " + set);
		inclusiveHeadSet.add(4);
		// headSet.add(75); // throws IllegalArgumentException because of out of range input
		System.out.println("set: " + set);

		SortedSet<Integer> subSet = set.subSet(5, 74);
		System.out.println("Before adding item to actual set, subSet: " + subSet);
		// subSet.add(2); // throws IllegalArgumentException because of out of range input

		// Adding element in backed set (original set) and see it getting reflected in subSet
		set.add(25);
		System.out.println("After adding item to actual set, subSet: " + subSet);
		System.out.println("Actual set : " + set);

		int polledFirst = set.pollFirst();
		System.out.println("Actual set after polledFirst : " + polledFirst + " is " + set);

		int polledLast = set.pollLast();
		System.out.println("Actual set after polledLast : " + polledLast + " is " + set);
	}

	public static void main(String[] args) {
		System.out.println("\nTesting HashSet interface...");
		hashSetDemo();
		System.out.println("\nTesting LinkedHashSet interface...");
		linkedHashSetDemo();
		System.out.println("\nTesting TreeSet interface...");
		treeSetDemo();
		System.out.println("\nTesting NavigableSet interface...");
		navigableSetDemo();
	}

}

class Book implements Comparable {
	private String title;
	private String author;
	private int year;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Book(String title, String author, int year) {
		super();
		this.title = title;
		this.author = author;
		this.year = year;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Book [title=" + title + ", author=" + author + ", year=" + year + "]";
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + year;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (year != other.year)
			return false;
		return true;
	}

	@Override
	public int compareTo(Object book) {
		System.out.println("Inside compareTo method of comparable interface");
		return getTitle().compareTo(((Book) book).getTitle());
	}
}

class TitleComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		System.out.println("Inside compare method of Comparator interface");
		if (o1 instanceof Book && o2 instanceof Book) {
			return ((Book) o1).getTitle().compareTo(((Book) o2).getTitle());
		}
		return 0;
	}

}
