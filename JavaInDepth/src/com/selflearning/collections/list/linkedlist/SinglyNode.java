package com.selflearning.collections.list.linkedlist;

public class SinglyNode {
	int data;
	SinglyNode next;

	/**
	 * @return the data
	 */
	public int getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(int data) {
		this.data = data;
	}

	/**
	 * @return the next
	 */
	public SinglyNode getNext() {
		return next;
	}

	/**
	 * @param next
	 *            the next to set
	 */
	public void setNext(SinglyNode next) {
		this.next = next;
	}
}
