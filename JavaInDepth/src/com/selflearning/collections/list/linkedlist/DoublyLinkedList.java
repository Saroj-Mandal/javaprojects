package com.selflearning.collections.list.linkedlist;

public class DoublyLinkedList {
	DoublyNode head = new DoublyNode();

	public void add(int data) {
		DoublyNode newNode = new DoublyNode();
		newNode.setData(data);

		// setting outgoing links
		newNode.setNext(head);
		newNode.setPrevious(head.getPrevious());

		// setting incoming links
		head.getPrevious().setNext(newNode);
		head.setPrevious(newNode);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
