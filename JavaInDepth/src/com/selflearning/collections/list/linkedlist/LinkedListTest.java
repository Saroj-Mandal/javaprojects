package com.selflearning.collections.list.linkedlist;

public class LinkedListTest {
	public static void main(String[] args) {

		SinglyLinkedList singlyLinkedList = new SinglyLinkedList();
		singlyLinkedList.add(1);
		singlyLinkedList.add(2);
		singlyLinkedList.add(3);
		singlyLinkedList.add(4);
		System.out.println(singlyLinkedList);

		DoublyLinkedList doublyLinkedList = new DoublyLinkedList();
		doublyLinkedList.add(5);
		doublyLinkedList.add(6);
		doublyLinkedList.add(7);
		doublyLinkedList.add(8);
		System.out.println(doublyLinkedList);
	}
}
