package com.selflearning.collections.list.linkedlist;

public class DoublyNode {
	int data;
	DoublyNode next;
	DoublyNode previous;

	/**
	 * @return the data
	 */
	public int getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(int data) {
		this.data = data;
	}

	/**
	 * @return the next
	 */
	public DoublyNode getNext() {
		return next;
	}

	/**
	 * @param next
	 *            the next to set
	 */
	public void setNext(DoublyNode next) {
		this.next = next;
	}

	/**
	 * @return the previous
	 */
	public DoublyNode getPrevious() {
		return previous;
	}

	/**
	 * @param previous
	 *            the previous to set
	 */
	public void setPrevious(DoublyNode previous) {
		this.previous = previous;
	}
}
