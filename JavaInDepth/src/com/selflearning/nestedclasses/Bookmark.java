package com.selflearning.nestedclasses;

import java.io.Serializable;
import java.util.Comparator;

public class Bookmark {
	private long id;
	private String title;
	private double rating;

	public static Comparator<Bookmark> RATTING_COMPARATOR_DESC = new RatingComparator();

	public static class RatingComparator implements Comparator<Bookmark>, Serializable {

		@Override
		public int compare(Bookmark o1, Bookmark o2) {
			// TODO Auto-generated method stub
			return o1.getRating() < o2.getRating() ? 1 : -1;
		}

	}

	public static class ComparatorList {
		public static class RatingComparator implements Comparator<Bookmark>, Serializable {

			@Override
			public int compare(Bookmark o1, Bookmark o2) {
				// TODO Auto-generated method stub
				return o1.getRating() < o2.getRating() ? -1 : 1;
			}

		}

		public static class TitleLengthComparator implements Comparator<Bookmark>, Serializable {

			@Override
			public int compare(Bookmark o1, Bookmark o2) {
				// TODO Auto-generated method stub
				return o1.getTitle().length() - o2.getTitle().length();
			}

		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Bookmark [id=").append(id).append(", title=").append(title).append(", rating=").append(rating).append("]");
		return builder.toString();
	}

}
