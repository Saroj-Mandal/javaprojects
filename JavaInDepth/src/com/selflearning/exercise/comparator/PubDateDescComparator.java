package com.selflearning.exercise.comparator;

import java.util.Comparator;

public class PubDateDescComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		int flag = ((Integer) ((Book) o1).getYear()).compareTo(((Integer) ((Book) o2).getYear()));
		if (flag == 0) {
			return ((Book) o1).getTitle().compareTo(((Book) o2).getTitle());
		} else {
			return flag * -1;
		}
	}
}