package com.selflearning.exercise;

public class StudentGradeUtil {

	public static void main(String[] args) {

		int[] studentIdList = { 1001, 1002 };
		char[][] studentsGrades = { { 'A', 'A', 'A', 'B' }, { 'A', 'B', 'B' } };
		int[] finalList = getStudentsByGPA(3.2, 3.5, studentIdList, studentsGrades);

		System.out.println("Final students length : " + finalList.length);

	}

	public static double[] calculateGPA(int[] studentIdList, char[][] studentsGrades) {
		double[] gpa = new double[studentsGrades.length];
		for (int i = 0; i < studentsGrades.length; i++) {
			double gradeTotal = 0;
			System.out.println(studentsGrades[i]);
			for (char grade : studentsGrades[i]) {
				switch (grade) {
				case 'A':
					gradeTotal += 4;
					break;
				case 'B':
					gradeTotal += 3;
					break;
				case 'C':
					gradeTotal += 2;
					break;
				}
			}
			System.out.println("Total grade of student " + studentIdList[i] + " is : " + gradeTotal);
			gpa[i] = gradeTotal / studentsGrades[i].length;
			System.out.println("GPA of student " + studentIdList[i] + " is : " + gpa[i]);
		}
		return gpa;
	}

	public static int[] getStudentsByGPA(double lower, double higher, int[] studentIdList, char[][] studentsGrades) {
		System.out.println("Input: lower = " + lower + " higher = " + higher + " studentIdList = " + studentIdList + " studentsGrades = "
				+ studentsGrades);
		// perform parameter validation. Return null if passed parameters are not valid
		if (lower < 0 || higher < 0 || lower > higher) {
			return null;
		}
		// invoke calculateGPA
		double[] gpa = calculateGPA(studentIdList, studentsGrades);
		System.out.println("GPA of students is : " + gpa);
		// construct the result array and return it. You would need an extra for loop to compute the size of the resulting array
		int[] finalStudentIds = new int[studentIdList.length];
		int index = 0;
		int finalLength = 0;
		for (int i = 0; i < gpa.length; i++) {
			if (gpa[i] >= lower && gpa[i] <= higher) {
				finalStudentIds[index] = studentIdList[i];
				finalLength++;
			}
		}
		System.out.println("Students falls in the range of higher >= student >= lower : " + finalStudentIds);
		int[] finalStudentwithInRange = new int[finalLength];
		index = 0;
		for (int studentId : finalStudentIds) {
			if (studentId > 0) {
				finalStudentwithInRange[index] = studentId;
			}
		}
		return finalStudentwithInRange;
	}

}
