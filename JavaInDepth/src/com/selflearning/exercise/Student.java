package com.selflearning.exercise;

public class Student extends Person {
	char grade;
	static int noOfStudent;

	Student(int id) {
		super(id);
		noOfStudent++;
	}

	Student(int id, String name) {
		super(id, name);
	}

	Student(int id, String name, char grade) {
		super(id, name);
		this.grade = grade;
	}

	public Student updateProfile(String name, char grade) {
		this.name = name;
		this.grade = grade;
		return this;
	}

	public Person updateProfile(char grade) {
		this.grade = grade;
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Student [grade=" + grade + ", id=" + id + ", name=" + name + "]";
	}

	public static void printPerson() {
		System.out.println("This is student class");
	}

}
