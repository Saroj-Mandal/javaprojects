package com.selflearning.exercise;

import java.util.Scanner;

public class ScannerTest {
	static {
		if (true) {
			System.out.println("Static block");
		}
	}

	{
		System.out.println("Anonymous block");
	}

	public static void main(String[] args) {
		ScannerTest ab = new ScannerTest();
		ScannerTest ab1 = new ScannerTest();
		System.out.println("Main method start");
		int i = 4;
		double d = 4.0;
		String s = "HackerRank ";

		Scanner scan = new Scanner(System.in);

		/* Declare second integer, double, and String variables. */
		int a = Integer.parseInt(scan.nextLine());
		double b = Double.parseDouble(scan.nextLine());
		/*
		 * nextLine() doesn't work properly(as we think) followed by nextX(nextInt() etc), as nextX() method is reads only that
		 * portion of the element ignoring remaining char.
		 */
		// sample input [1 2.3 saroj mandal]
		// int a = scan.nextInt();
		// double b = scan.nextDouble();
		String c = scan.nextLine();

		/* Read and save an integer, double, and String to your variables. */
		// Note: If you have trouble reading the entire String, please go back and review the Tutorial closely.

		/* Print the sum of both integer variables on a new line. */
		System.out.println(i + a);
		/* Print the sum of the double variables on a new line. */
		System.out.println(d + b);
		/*
		 * Concatenate and print the String variables on a new line; the 's' variable above should be printed first.
		 */
		System.out.println(s + c);
		scan.close();
	}
}
