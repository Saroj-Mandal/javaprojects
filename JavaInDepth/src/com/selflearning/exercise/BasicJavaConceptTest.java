package com.selflearning.exercise;

public class BasicJavaConceptTest {
	int id;
	int id1 = id; /* id3 = id2; */
	int id2;

	public static void main(String[] args) {
		constructorChaining();

		// System.out.println(this.id); //can't use this in static context.
		arrithmaticOparatorTest();

		// logical operator test
		logicalOperatorTest();

		switchExample();

		forLoopTest();

		labeledBreakTest();

		labeledContineTest();

		garbageCollectionTest();

	}

	public static void garbageCollectionTest() {
		Student[] students = { new Student(1000), new Student(1001), new Student(1002) };
		removeLast(students);
		Student student = students[0];
		students[0] = null;
		System.out.println("student" + student);
		System.out.println("student array" + students[0] + students[1] + students[2]);
	}

	public static void removeLast(Student[] students) {
		students[students.length - 1] = null;
	}

	private static void constructorChaining() {
		Student s1 = new Student(1000);
		Student s2 = new Student(1001, "Saroj");
		Student s3 = new Student(1002, "Saroj", 'A');
		Person s4 = new Student(1000);

		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);

		s1.updateProfile("Tonny");
		s1.grade = 'C';
		System.out.println(s1);
		s2.updateProfile('B');

		s4.printPerson();

		System.out.println(s1.updateProfile("Vini", 'F'));
	}

	private static void logicalOperatorTest() {
		System.out.println("\n Inside logicalOperatorTest Example");
		Student s4 = new Student(1002, "Saroj", 'A');
		Student s5 = new Student(1002, "Saroj", 'A');
		System.out.println("Student 4 and Student 5 are referencing same object : " + (s4 == s5));
		System.out.println("Student 4 and Student 5 are referencing different object : " + (s4 != s5));
		Student s6 = s5;
		System.out.println("Student 5 and Student 6 are referencing same object : " + (s5 == s6));
	}

	private static void arrithmaticOparatorTest() {
		System.out.println("\n Inside arrithmaticOparatorTest Example");
		char var1 = 50; // will be assigned corresponding UTF16 value 2
		char var2 = 97;
		System.out.println("Var1 is : " + var1 + ", and var2 is : " + var2); // will print Var1 is : 2, and var2 is : a
		System.out.println("(Var1 + var2) is : " + (var1 + var2)); // (Var1 + var2) is : 147 promoted to decimal equivalent int value
		System.out.println("(Var1 + 4) is : " + (var1 + 4)); // (Var1 + 4) is : 54 here var1 is promoted to int
		System.out.println("Var1 + 4 is : " + var1 + 4); // Var1 + 4 is : 24 as there is no parenthesis it simply concatenate the values
		System.out.println("(var1 - 'a') is : " + (var1 - 'a')); // (var1 - 'a') is : -47 char 'a' and var1 promoted to int
		System.out.println("(var1 + 'a') is : " + ('b' + 'a')); // (var1 + 'a') is : 147 both 'b' and 'a' promoted to int
	}

	private static void switchExample() {
		System.out.println("\n Inside switch Example");
		switchExample((byte) 5);
		byte month = (byte) 4;
		final byte month1 = 23;
		switch (month) {
		case 1:
			System.out.println("1 is satisfied");
			break;
		case 3:
			System.out.println("3 is satisfied");
			break;
		case (byte) 128:
			System.out.println("128 is satisfied" + month);
			break;
		case month1:
			System.out.println("final variable 23 is satisfied" + month);
			break;
		default:
			System.out.println("Default is satisfied" + month);
			break;
		case 4:
			System.out.println("4 is satisfied" + month);
			break;
		}
	}

	private static void switchExample(final int month1) {
		System.out.println("\n Inside switch Example");
		byte month = 4;
		switch (month) {
		case 1:
			System.out.println("128 is satisfied" + month);
			break;
		// case month1: //can't use month1 here as it's not initialized.
		// System.out.println("final variable 23 is satisfied" + month);
		// break;
		default:
			System.out.println("Default is satisfied" + month);
			break;
		}
	}

	private static void forLoopTest() {
		System.out.println("\n Inside forLoopTest Example");
		for (int i = 0; i < 10; i++) {
			System.out.println("i " + i);
		}
		int j = 0;
		for (j = 11; j < 20; j++) {
			System.out.println("j " + j);
		}
		int k = 0, l = 0;
		for (k = 21, l = 31; k < 30; k++, l++) {
			System.out.println("k " + k + " l " + l);
		}

		for (System.out.println("at initialization block" + k), k = 41, l = 45; k < 47 || l < 49; k++, l++) {
			System.out.println("k " + k + " l " + l);
		}

		for (System.out.println("at initialization block, calling other method and k= " + k), switchExample(1), k = 41, l = 45; k < 47
				|| l < 49; System.out.println("k " + k++ + " l " + l++))
			;
	}

	private static void labeledBreakTest() {
		System.out.println("\n Inside labeledBreakTest Example");
		int num = 0;
		outerfoorLoop: for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (i == 5 && j == 5) {
					break outerfoorLoop;
					// break outerfoorLoop1; this break statement is not enclosed in outerfoorLoop1 label block
				}
				num++;
			}
		}
		outerfoorLoop1: for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (i == 5 && j == 5) {
					break;
				}
				// num++;
			}
		}
		if (true) {
			// break; outside loop
		}
		int num1 = 0;
		ifLbl: if (true) {
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 10; j++) {
					if (i == 5 && j == 5) {
						break ifLbl;
					}
					num1++;
				}
			}
		}

		System.out.println("Number is : " + num);
		System.out.println("Number1 is : " + num1);
	}

	private static void labeledContineTest() {
		System.out.println("\n Inside labeledContineTest Example");
		int num = 0;
		outerfoorLoop: for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (i == 5 && j == 5) {
					continue outerfoorLoop;
					// break outerfoorLoop1; this break statement is not enclosed in outerfoorLoop1 label block
				}
				num++;
			}
		}
		outerfoorLoop1: for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (i == 5 && j == 5) {
					break;
				}
				// num++;
			}
		}

		int num1 = 0;
		ifLbl: if (true) {
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 10; j++) {
					if (i == 5 && j == 5) {
						// continue ifLbl; can't use outside loop
					}
					num1++;
				}
			}
		}
		System.out.println("Number is : " + num);
		System.out.println("Number1 is : " + num1);
	}
}
