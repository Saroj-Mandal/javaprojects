package com.selflearning.exercise;

public class PassByRefValue {

	static PassByRefValue passByRefValue = new PassByRefValue();

	public static void main(String[] args) {
		Integer x = new Integer(1);
		Integer y = new Integer(2);
		swap(x, y);
		System.out.println("x = " + x + " y = " + y);
		int c = 3;
		int d = 4;
		swap(c, d);
		System.out.println("c = " + c + " d = " + d);

		String e = new String("First");
		String f = new String("Second");
		swap(e, f);
		System.out.println("e = " + e + " f = " + f);

		Person saroj = passByRefValue.new Person();
		Person tonny = passByRefValue.new Person();
		saroj.setName("Saroj");
		saroj.setAge(90);
		tonny.setName("Tonny");
		tonny.setAge(80);
		System.out.println("saroj = " + saroj + " tonny = " + tonny);
		swap(saroj, tonny);
		System.out.println("saroj = " + saroj + " tonny = " + tonny);

		assignmentTest();
	}

	private static void swap(Integer param1, Integer param2) {
		Integer temp = param1;
		param1 = param2;
		param2 = temp;
		System.out.println("param1 = " + param1 + " param2 = " + param2);
	}

	private static void swap(int param1, int param2) {
		int temp = param1;
		param1 = param2;
		param2 = temp;
		System.out.println("param1 = " + param1 + " param2 = " + param2);
	}

	private static void swap(String param1, String param2) {
		String temp = param1;
		param1 = param2;
		param2 = temp;
		System.out.println("param1 = " + param1 + " param2 = " + param2);
	}

	private static void swap(Person person1, Person person2) {
		String name = person1.getName();
		int age = person1.getAge();
		person1.setName(person2.getName());
		person1.setAge(person2.getAge());
		person2.setName(name);
		person2.setAge(age);
		// person1 = null;
		// person1 = person2;
		System.out.println("person1 = " + person1 + " person2 = " + person2);

	}

	private static void assignmentTest() {
		int oldInt = 10;
		int newInt = oldInt;
		newInt = 20;
		System.out.println("Current value of oldInt is : " + oldInt);

		// passing primitive
		System.out.println("\nPrimitive test ... ");
		int id = 2000;
		Student student1 = passByRefValue.new Student();
		student1.updateId(id);
		System.out.println("ID in User: " + id);

		System.out.println("\nObject Reference test ... ");
		Student student2 = passByRefValue.new Student();
		System.out.println("ID before updating: " + student2.id);
		updateId(student2, id);
		System.out.println("ID after updating: " + student2.id);
	}

	static void updateId(Student student, int id) {
		student.id = id + 100;
	}

	class Person {
		private String name;
		private int age;

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name
		 *            the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @return the age
		 */
		public int getAge() {
			return age;
		}

		/**
		 * @param age
		 *            the age to set
		 */
		public void setAge(int age) {
			this.age = age;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "Person [name=" + name + ", age=" + age + "]";
		}
	}

	// Student.java
	public class Student {
		int id = 0;

		void updateId(int newId) {
			id = newId + 100;
			System.out.println("ID in Student: " + id);
		}
	}
}
