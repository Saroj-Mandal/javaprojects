package com.selflearning.exercise.reservation;

public class ReservationTest {

	public static void main(String[] args) {
		TicketReservation ticketReservation = new TicketReservation();
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "1"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "2"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "3"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "4"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "5"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "6"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "7"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "8"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "9"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "10"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "11"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "12"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "13"));
		System.out.println(ticketReservation.bookFlight("Saroj", "Mandal", 47, "Male", "Economy", "14"));
	
		System.out.println("Confirmed List Size : " + ticketReservation.getConfirmedList().size());
		System.out.println("Waiting List Size : " + ticketReservation.getWaitingList().size());
		
		System.out.println(ticketReservation.cancel("1"));
		System.out.println(ticketReservation.cancel("2"));
		System.out.println(ticketReservation.cancel("3"));
		System.out.println(ticketReservation.cancel("5"));
		System.out.println(ticketReservation.cancel("4"));
		System.out.println(ticketReservation.cancel("6"));
		
		System.out.println("Confirmed List Size : " + ticketReservation.getConfirmedList().size());
		System.out.println("Waiting List Size : " + ticketReservation.getWaitingList().size());
	}

}
