package com.selflearning.exercise.reservation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;

public class TicketReservation {

	private static final int CONFIRMEDLIST_LIMIT = 10;
	private static final int WAITINGLIST_LIMIT = 3;

	private List<Passenger> confirmedList = new ArrayList<>();
	private Deque<Passenger> waitingList = new ArrayDeque<>();

	// This getter is used only by the junit test case.
	public List<Passenger> getConfirmedList() {
		return confirmedList;
	}

	// This getter is used only by the junit test case.
	public Deque<Passenger> getWaitingList() {
		return waitingList;
	}

	/**
	 * Returns true if passenger could be added into either confirmedList or
	 * waitingList. Passenger will be added to waitingList only if confirmedList is
	 * full.
	 * 
	 * Return false if both passengerList & waitingList are full
	 */
	public boolean bookFlight(String firstName, String lastName, int age, String gender, String travelClass,
			String confirmationNumber) {
		if (getConfirmedList().size() < CONFIRMEDLIST_LIMIT) {
			Passenger passenger = new Passenger(firstName, lastName, age, gender, travelClass, confirmationNumber);
			getConfirmedList().add(passenger);
			return true;
		} else if (getWaitingList().size() < WAITINGLIST_LIMIT) {
			Passenger passenger = new Passenger(firstName, lastName, age, gender, travelClass, confirmationNumber);
			getWaitingList().add(passenger);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Removes passenger with the given confirmationNumber. Passenger could be in
	 * either confirmedList or waitingList. The implementation to remove the
	 * passenger should go in removePassenger() method and removePassenger method
	 * will be tested separately by the uploaded test scripts.
	 * 
	 * If passenger is in confirmedList, then after removing that passenger, the
	 * passenger at the front of waitingList (if not empty) must be moved into
	 * passengerList. Use poll() method (not remove()) to extract the passenger from
	 * waitingList.
	 */
	public boolean cancel(String confirmationNumber) {
		boolean isRemovedFromWaitingList = removePassenger(getWaitingList().iterator(), confirmationNumber);
		if (!isRemovedFromWaitingList) {
			if (removePassenger(getConfirmedList().iterator(), confirmationNumber)) {
				if (!getWaitingList().isEmpty()) {
					System.out.println("Moved passenger from waiting to confirmed list with confirmation number : "
							+ confirmationNumber);
					getConfirmedList().add(getWaitingList().poll());
				}
				System.out.println(
						"Removed passenger from confirmed list with confirmation number : " + confirmationNumber);
				return true;
			}
		} else {
			System.out.println("Removed passenger from waiting list with confirmation number : " + confirmationNumber);
			return true;
		}
		return false;
	}

	/**
	 * Removes passenger with the given confirmation number. Returns true only if
	 * passenger was present and removed. Otherwise, return false.
	 */
	public boolean removePassenger(Iterator<Passenger> iterator, String confirmationNumber) {
		while (iterator.hasNext()) {
			Passenger passenger = iterator.next();
			if (confirmationNumber.equalsIgnoreCase(passenger.getConfirmationNumber())) {
				iterator.remove();
				System.out.println("Confirmed list size = " + getConfirmedList().size() + " Waiting list size : "
						+ getWaitingList().size());
				return true;
			}
		}
		return false;
	}

}