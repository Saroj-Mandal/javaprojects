package com.selflearning.exercise;

public class OverloadingTest {

	public static void main(String[] args) {
		go(false);
		go(true, 23);
		byte b = 45;
		short s = 127;
		long l = 823840;
		go(false, b);
		go(false, s);
		go(false, l);
		go(false, new int[] { 127 });
		go(false, new int[3]);
		go(false, 1, 2);
		go(false, 1, 2, 3);
	}

	static private void go(boolean b, int i) {
		System.out.println("Inside Go with one boolean and 1 int parameter");
	}

	static private void go(boolean b, short s) {
		System.out.println("Inside Go with one boolean and 1 short parameter");
	}

	static private void go(boolean b, byte by) {
		System.out.println("Inside Go with one boolean and 1 byte parameter");
	}

	static private void go(boolean b, long l) {
		System.out.println("Inside Go with one boolean and 1 long parameter");
	}

	static private void go(boolean b, int... i) {
		System.out.println("Inside Go with one boolean and 1 varargs parameter, param length = " + i.length);
	}

	static private void go(boolean b, int i, int j, int k) {
		System.out.println("Inside Go with one boolean and 3 int parameter");
	}

	static private void go(boolean b, int i, int k) {
		System.out.println("Inside Go with one boolean and 2 int parameter");
	}

}
