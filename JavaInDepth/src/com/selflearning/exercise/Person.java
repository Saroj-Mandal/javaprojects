package com.selflearning.exercise;

import java.util.HashSet;
import java.util.Set;

public class Person implements DemoInterface {
	static {
		System.out.println("Lol");
	}
	int id;
	String name;

	Person(int id) {
		super();
		this.id = id;
	}

	Person(int id, String name) {
		this(id);
		this.name = name;
	}

	public Person updateProfile(String name) {
		this.name = name;
		return this;
	}
	Set s = new HashSet<>();

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + "]";
	}

	public static void printPerson() {
		System.out.println("This is person class");
	}

}
