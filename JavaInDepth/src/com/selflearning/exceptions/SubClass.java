package com.selflearning.exceptions;

import java.io.FileNotFoundException;
import java.io.IOException;

public class SubClass extends SuperClass {
	@Override
	public void PrintMyName(String name) {
		System.out.println("My Name is : " + name);
	}

	@Override
	public void PrintMyName(String firstName, String lastName) throws FileNotFoundException {
		System.out.println("My first name is : " + firstName + " and last name is : " + lastName);
	}
}
