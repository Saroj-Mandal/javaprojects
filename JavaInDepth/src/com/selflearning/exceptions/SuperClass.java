package com.selflearning.exceptions;

import java.io.FileNotFoundException;
import java.io.IOException;

public class SuperClass {
	public void PrintMyName(String name) throws RuntimeException {
		System.out.println("My Name is : " + name);
	}

	public void PrintMyName(String firstName, String lastName)throws IOException {
		System.out.println("My first name is : " + firstName + " and last name is : " + lastName);
	}
}
