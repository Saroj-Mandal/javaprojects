package com.selflearning.io;

import java.io.Serializable;

public class NonSerializedClass implements Serializable {
	public NonSerializedClass() {
		System.out.println("NonSerializedClass: Constructor Invoked");
	}

	public NonSerializedClass(String name) {
		super();
		this.name = name;
		System.out.println("NonSerializedClass: Parameterized Constructor Invoked");
	}

	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("Person [name=%s]", name);
	}
}
