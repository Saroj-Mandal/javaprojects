package com.selflearning.io;

import java.io.Serializable;

public class SerializedSubClass extends SerializedSuperClass implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5461780705649145585L;


	public SerializedSubClass(String name, int intVar, long longVar, double doubleVar, char charVar, short shortVar, byte byteVar,
			boolean booleanVar, float floatVar, String trName, int trIntVar, long trLongVar, double trDoubleVar, char trCharVar,
			short trShortVar, byte trByteVar, boolean trBooleanVar, float trFloatVar) {
		super();
		this.name = name;
		this.intVar = intVar;
		this.longVar = longVar;
		this.doubleVar = doubleVar;
		this.charVar = charVar;
		this.shortVar = shortVar;
		this.byteVar = byteVar;
		this.booleanVar = booleanVar;
		this.floatVar = floatVar;
		this.trName = trName;
		this.trIntVar = trIntVar;
		this.trLongVar = trLongVar;
		this.trDoubleVar = trDoubleVar;
		this.trCharVar = trCharVar;
		this.trShortVar = trShortVar;
		this.trByteVar = trByteVar;
		this.trBooleanVar = trBooleanVar;
		this.trFloatVar = trFloatVar;
		System.out.println("SerializedSubClass: parameterized Constructor Invoked");
	}

	public SerializedSubClass() {
		System.out.println("SerializedSubClass: Constructor Invoked");
	}

	private String name = "India";
	private int intVar = 1000;
	private long longVar = 200000;
	private double doubleVar = 29834.2D;
	private char charVar = 'A';
	private short shortVar = 12832;
	private byte byteVar = 127;
	private boolean booleanVar = true;
	private float floatVar = 213.21F;
	private transient String trName = "Bangladesh";
	private transient int trIntVar = -198237;
	private transient long trLongVar = -981273;
	private transient double trDoubleVar = -1238.32D;
	private transient char trCharVar = 'C';
	private transient short trShortVar = -1243;
	private transient byte trByteVar = -127;
	private transient boolean trBooleanVar = true;
	private transient float trFloatVar = -1721.23F;

	private int version;
	

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the intVar
	 */
	public int getIntVar() {
		return intVar;
	}

	/**
	 * @param intVar
	 *            the intVar to set
	 */
	public void setIntVar(int intVar) {
		this.intVar = intVar;
	}

	/**
	 * @return the longVar
	 */
	public long getLongVar() {
		return longVar;
	}

	/**
	 * @param longVar
	 *            the longVar to set
	 */
	public void setLongVar(long longVar) {
		this.longVar = longVar;
	}

	/**
	 * @return the doubleVar
	 */
	public double getDoubleVar() {
		return doubleVar;
	}

	/**
	 * @param doubleVar
	 *            the doubleVar to set
	 */
	public void setDoubleVar(double doubleVar) {
		this.doubleVar = doubleVar;
	}

	/**
	 * @return the charVar
	 */
	public char getCharVar() {
		return charVar;
	}

	/**
	 * @param charVar
	 *            the charVar to set
	 */
	public void setCharVar(char charVar) {
		this.charVar = charVar;
	}

	/**
	 * @return the shortVar
	 */
	public short getShortVar() {
		return shortVar;
	}

	/**
	 * @param shortVar
	 *            the shortVar to set
	 */
	public void setShortVar(short shortVar) {
		this.shortVar = shortVar;
	}

	/**
	 * @return the byteVar
	 */
	public byte getByteVar() {
		return byteVar;
	}

	/**
	 * @param byteVar
	 *            the byteVar to set
	 */
	public void setByteVar(byte byteVar) {
		this.byteVar = byteVar;
	}

	/**
	 * @return the booleanVar
	 */
	public boolean isBooleanVar() {
		return booleanVar;
	}

	/**
	 * @param booleanVar
	 *            the booleanVar to set
	 */
	public void setBooleanVar(boolean booleanVar) {
		this.booleanVar = booleanVar;
	}

	/**
	 * @return the floatVar
	 */
	public float getFloatVar() {
		return floatVar;
	}

	/**
	 * @param floatVar
	 *            the floatVar to set
	 */
	public void setFloatVar(float floatVar) {
		this.floatVar = floatVar;
	}

	/**
	 * @return the trIntVar
	 */
	public int getTrIntVar() {
		return trIntVar;
	}

	/**
	 * @param trIntVar
	 *            the trIntVar to set
	 */
	public void setTrIntVar(int trIntVar) {
		this.trIntVar = trIntVar;
	}

	/**
	 * @return the trLongVar
	 */
	public long getTrLongVar() {
		return trLongVar;
	}

	/**
	 * @param trLongVar
	 *            the trLongVar to set
	 */
	public void setTrLongVar(long trLongVar) {
		this.trLongVar = trLongVar;
	}

	/**
	 * @return the trDoubleVar
	 */
	public double getTrDoubleVar() {
		return trDoubleVar;
	}

	/**
	 * @param trDoubleVar
	 *            the trDoubleVar to set
	 */
	public void setTrDoubleVar(double trDoubleVar) {
		this.trDoubleVar = trDoubleVar;
	}

	/**
	 * @return the trCharVar
	 */
	public char getTrCharVar() {
		return trCharVar;
	}

	/**
	 * @param trCharVar
	 *            the trCharVar to set
	 */
	public void setTrCharVar(char trCharVar) {
		this.trCharVar = trCharVar;
	}

	/**
	 * @return the trShortVar
	 */
	public short getTrShortVar() {
		return trShortVar;
	}

	/**
	 * @param trShortVar
	 *            the trShortVar to set
	 */
	public void setTrShortVar(short trShortVar) {
		this.trShortVar = trShortVar;
	}

	/**
	 * @return the trByteVar
	 */
	public byte getTrByteVar() {
		return trByteVar;
	}

	/**
	 * @param trByteVar
	 *            the trByteVar to set
	 */
	public void setTrByteVar(byte trByteVar) {
		this.trByteVar = trByteVar;
	}

	/**
	 * @return the trBooleanVar
	 */
	public boolean isTrBooleanVar() {
		return trBooleanVar;
	}

	/**
	 * @param trBooleanVar
	 *            the trBooleanVar to set
	 */
	public void setTrBooleanVar(boolean trBooleanVar) {
		this.trBooleanVar = trBooleanVar;
	}

	/**
	 * @return the trFloatVar
	 */
	public float getTrFloatVar() {
		return trFloatVar;
	}

	/**
	 * @param trFloatVar
	 *            the trFloatVar to set
	 */
	public void setTrFloatVar(float trFloatVar) {
		this.trFloatVar = trFloatVar;
	}

	/**
	 * @return the trName
	 */
	public String getTrName() {
		return trName;
	}

	/**
	 * @param trName
	 *            the trName to set
	 */
	public void setTrName(String trName) {
		this.trName = trName;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"SerializedSubClass [name=%s,\n intVar=%s,\n longVar=%s,\n doubleVar=%s,\n charVar=%s,\n shortVar=%s,\n byteVar=%s,\n booleanVar=%s,\n floatVar=%s,\n trName=%s,\n trIntVar=%s,\n trLongVar=%s,\n trDoubleVar=%s,\n trCharVar=%s,\n trShortVar=%s,\n trByteVar=%s,\n trBooleanVar=%s,\n trFloatVar=%s]",
				name, intVar, longVar, doubleVar, charVar, shortVar, byteVar, booleanVar, floatVar, trName, trIntVar, trLongVar,
				trDoubleVar, trCharVar, trShortVar, trByteVar, trBooleanVar, trFloatVar);
	}

}
