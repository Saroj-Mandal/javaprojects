package com.selflearning.io;

import java.io.Serializable;

public class SerializedSuperClass implements Serializable {
	public SerializedSuperClass() {
		System.out.println("SerializedSubClass: Constructor Invoked");
	}

	private NonSerializedClass nonSerializedClass;

	/**
	 * @return the person
	 */
	public NonSerializedClass getNonSerializedClass() {
		return nonSerializedClass;
	}

	/**
	 * @param nonSerializedClass
	 *            the person to set
	 */
	public void setNonSerializedClass(NonSerializedClass nonSerializedClass) {
		this.nonSerializedClass = nonSerializedClass;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("SerializedSubClass [nonSerializedClass=%s]", nonSerializedClass);
	}

}
