package com.selflearning.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Scanner;

public class IODemo {
	public static void applyEncoding() {
		System.out.println("\nInside applyEncoding ...");
		// System.out.println("Default Character Encoding: " + System.getProperty("file.encoding"));

		// Ensure Eclipse property is set as UTF8
		printEncodingDetails("saroj");
		printEncodingDetails("�"); // Euro (Reference:
									// http://stackoverflow.com/questions/34922333/how-does-java-fit-a-3-byte-unicode-character-into-a-char-type)
		printEncodingDetails("\u1F602"); // Non-BMP Unicode Code Point ~ Tears of Joy Emoji (one of Smiley graphic symbol)
	}

	private static void printEncodingDetails(String symbol) {
		System.out.println("\nSymbol: " + symbol);
		try {
			System.out.println("ASCII: " + Arrays.toString(symbol.getBytes("US-ASCII")));
			System.out.println("ISO-8859-1: " + Arrays.toString(symbol.getBytes("ISO-8859-1")));
			System.out.println("UTF-8: " + Arrays.toString(symbol.getBytes("UTF-8")));
			System.out.println("UTF-16: " + Arrays.toString(symbol.getBytes("UTF-16")));
			System.out.println("UTF-16 BE: " + Arrays.toString(symbol.getBytes("UTF-16BE")));
			System.out.println("UTF-16 LE: " + Arrays.toString(symbol.getBytes("UTF-16LE")));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
	}

	private static void fileCopyWithoutBuffer() {
		System.out.println("Copying file without using buffer ... ");
		File inputFile = new File(System.getProperty("user.dir") + "\\assets\\sample.mp4");
		File outputFile = new File(System.getProperty("user.dir") + "\\assets\\sample-out-non-buff.mp4");
		System.out.println("Copying file " + inputFile.getName() + " with size : " + inputFile.length() + " bytes");
		long startTime = 0, elapsedTime = 0;
		try (FileInputStream in = new FileInputStream(inputFile); FileOutputStream out = new FileOutputStream(outputFile)) {
			int byteRead;
			startTime = System.nanoTime();
			while ((byteRead = in.read()) != -1) {
				out.write(byteRead);
			}
			elapsedTime = System.nanoTime();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("File copied to " + outputFile.getName() + " with size : " + outputFile.length() + " bytes");
		System.out.println("Total time taken to copy file is : " + ((elapsedTime - startTime) / 1000000) + " milliseconds");

	}

	private static void fileCopyWithBuffer() {
		System.out.println("Copying file using buffer ... ");
		File inputFile = new File(System.getProperty("user.dir") + "\\assets\\sample.mp4");
		File outputFile = new File(System.getProperty("user.dir") + "\\assets\\sample-out-buff.mp4");
		System.out.println("Copying file " + inputFile.getName() + " with size : " + inputFile.length() + " bytes");
		long startTime = 0, elapsedTime = 0;
		try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(inputFile));
				BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(outputFile))) {
			int noOfByteRead;
			startTime = System.nanoTime();
			byte[] buffer = new byte[4000];
			while ((noOfByteRead = in.read(buffer)) != -1) {
				out.write(buffer, 0, noOfByteRead);
			}
			elapsedTime = System.nanoTime();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("File copied to " + outputFile.getName() + " with size : " + outputFile.length() + " bytes");
		System.out.println("Total time taken to copy file is : " + ((elapsedTime - startTime) / 1000000) + " milliseconds");

	}

	private static void readFromStandardInput() {
		System.out.println("\nInside readFromStandardInput ...");
		String data;
		System.out.print("Enter \"start\" to continue (Using BufferedReader): ");

		/*
		 * try (BufferedReader in = new BufferedReader(new InputStreamReader(System.in, "UTF-8"))) { while ((data =
		 * in.readLine()) != null && !data.equals("start")) { System.out.print("\nDid not enter \"start\". Try again: "); } }
		 * catch (IOException e) { e.printStackTrace(); } System.out.println("Correct!!");
		 */
		System.out.print("\nEnter \"start\" to continue (Using java.util.Scanner): ");
		Scanner scanner = new Scanner(System.in);

		while (!(data = scanner.nextLine()).equals("start")) {
			System.out.print("\nDid not enter \"start\". Try again: ");
		}
		System.out.println("Correct!!");

		System.out.println("Now, enter the start code: ");
		int code = scanner.nextInt(); // other methods: nextLong, nextDouble, etc
		System.out.println("Thanks. You entered code: " + code);

		/**
		 * Scanner ~ a text scanner for parsing primitives & string ~ breaks its input into tokens using a delimited pattern
		 * (default: whitespace) ~ when System.in is used, internally constructor uses an InputStreamReader to read from it ~
		 * hasXXX & nextXXX can be used together ~ InputMismatchException is thrown ~ From Java 5 onwards
		 */

		Scanner s1 = new Scanner("Hello, How are you?");
		while (s1.hasNext()) {
			System.out.println(s1.next());
		}
	}

	private static void readTextFromFile() {

		File inputFile = new File(System.getProperty("user.dir") + "\\assets\\sample.txt");
		File outputFile = new File(System.getProperty("user.dir") + "\\assets\\sample-out.txt");
		try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)))) {
			String lineRead;
			while ((lineRead = in.readLine()) != null) {
				System.out.println("Character from the buffer : " + lineRead);
			}

			/*
			 * char[] cbuf = new char[4000]; int noOfCharRead; while ((noOfCharRead = in.read(cbuf)) != -1) { out.write(cbuf, 0,
			 * noOfCharRead); }
			 */

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void fileMethodsDemo() {
		System.out.println("\nInside fileMethodsDemo ...");

		File f = new File(System.getProperty("user.dir") + "\\assets\\sample.txt"); // "movies\\movies.txt" also works
		// File f = new File("walden.jpg");

		System.out.println("getAbsolutePath(): " + f.getAbsolutePath());
		try {
			System.out.println("getCanonicalPath(): " + f.getCanonicalPath());
			System.out.println("createNewFile(): " + f.createNewFile());
		} catch (IOException e) {
		}
		System.out.println("separator: " + f.separator);
		System.out.println("separatorChar: " + f.separatorChar);
		System.out.println("getParent(): " + f.getParent());
		System.out.println("lastModified(): " + f.lastModified());
		System.out.println("exists(): " + f.exists());
		System.out.println("isFile(): " + f.isFile());
		System.out.println("isDirectory(): " + f.isDirectory());
		System.out.println("length(): " + f.length());

		System.out.println("My working or user directory: " + System.getProperty("user.dir"));
		System.out.println("new File(\"testdir\").mkdir(): " + new File("testdir").mkdir());
		System.out.println("new File(\"testdir\\test\").mkdir(): " + new File("testdir\\test").mkdir());
		System.out.println("new File(\"testdir\").delete(): " + new File("testdir").delete());
		System.out.println("new File(\"testdir\\test\").delete(): " + new File("testdir\\test").delete());
		System.out.println("new File(\"testdir\\test1\\test2\").mkdir(): " + new File("testdir\\test1\\test2").mkdir());
		System.out.println("new File(\"testdir\\test1\\test2\").mkdirs(): " + new File("testdir\\test1\\test2").mkdirs());

		try {
			File f2 = new File("temp.txt");
			System.out.println("f2.createNewFile(): " + f2.createNewFile());
			System.out.println("f2.renameTo(...): " + f2.renameTo(new File("testdir\\temp1.txt"))); // move!!
		} catch (IOException e) {
		}

	}

	public static void dirFilter(boolean applyFilter) {
		System.out.println("\nInside dirFilter ...");

		File path = new File(System.getProperty("user.dir") + "\\assets");
		String[] list;

		if (!applyFilter)
			list = path.list();
		else
			list = path.list(new DirFilter());

		// Arrays.sort(list, String.CASE_INSENSITIVE_ORDER);
		for (String dirItem : list)
			System.out.println(dirItem);
	}

	private static void replicateFiles(String dir) {
		File path = new File(dir);
		String[] list = path.list();
		System.out.println("Starting replication");
		for (String file : list) {
			File inputFile = new File(dir + "\\" + file);
			System.out.println("creating copy of file : " + inputFile.getName());
			fileCopyWithBuffer(inputFile);
		}

	}

	private static void fileCopyWithBuffer(File inputFile) {
		System.out.println("Copying file using buffer ... ");
		File outputFile = new File(inputFile.getParent() + "\\rep-" + inputFile.getName());

		System.out.println("Copying file " + inputFile.getName() + " with size : " + inputFile.length() + " bytes");
		long startTime = 0, elapsedTime = 0;
		try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(inputFile));
				BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(outputFile))) {
			int noOfByteRead;
			System.out.println("outputFile" + outputFile.getCanonicalPath());
			startTime = System.nanoTime();
			byte[] buffer = new byte[4000];
			while ((noOfByteRead = in.read(buffer)) != -1) {
				out.write(buffer, 0, noOfByteRead);
			}
			elapsedTime = System.nanoTime();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("File copied to " + outputFile.getName() + " with size : " + outputFile.length() + " bytes");
		System.out.println("Total time taken to copy file is : " + ((elapsedTime - startTime) / 1000000) + " milliseconds");
	}

	private static SerializedSubClass getObjectToSerialize() {
		SerializedSubClass serObj = new SerializedSubClass();
		serObj.setNonSerializedClass(new NonSerializedClass("I am Don"));
		return serObj;
	}

	private static void serializationDemo() {
		SerializedSubClass serObj = getObjectToSerialize();
		System.out.println("\n Before Serialization object is : " + serObj);
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(System.getProperty("user.dir") + "\\data\\object.ser"))) {
			oos.writeObject(serObj);
			oos.writeObject("My Name is Khan");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Serialization done ! ");
	}

	private static void deserializationDemo() {
		try (ObjectInputStream oos = new ObjectInputStream(new FileInputStream(System.getProperty("user.dir") + "\\data\\object.ser"))) {
			System.out.println(oos.readObject());
			System.out.println(oos.readObject());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void encodingSync() {
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream(System.getProperty("user.dir") + "\\data\\encoding.txt"), "UTF-8"))) {
			System.out.println(br.readLine());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			System.out.println(new String("�".getBytes("UTF-8"), "UTF-16BE"));
			System.out.println(new String("a".getBytes("US-ASCII"), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		System.out.println(System.getProperty("user.dir"));
		// applyEncoding();
		// fileCopyWithoutBuffer();
		// fileCopyWithBuffer();
		// readTextFromFile();
		// readFromStandardInput();
		// fileMethodsDemo();
		// dirFilter(true);
		// replicateFiles(System.getProperty("user.dir") + "\\assets");
		// serializationDemo();
		// deserializationDemo();
		encodingSync();

	}

	class Test {

	}

}

class DirFilter implements FilenameFilter {
	// Holds filtering criteria
	@Override
	public boolean accept(File file, String name) {
		return name.endsWith(".mp4") || name.endsWith(".MP4");
	}

}