package com.selflearning.unit1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.selflearning.common.Condition;
import com.selflearning.common.Person;

public class Unit1ExceciseJava7 {

	public static void main(String[] args) {
		// Create a list with person object
		List<Person> peoples = Arrays.asList(new Person("Saroj", "Mandal", 28),
				new Person("Vinothini", "Narayanasamy", 23), new Person("Sakshi", "Srivastava", 26),
				new Person("Hemanth", "Neelam", 25), new Person("Ashutodh", "Mahato", 31),
				new Person("Jayaprakash", "Ethiraja", 32), new Person("Rajasekhar", "Kendole", 32),
				new Person("Heena", "Srivastava", 28), new Person("Vijaya", "Kumar", 28));
		// Sort list by last name
		peoples.sort(new Comparator<Person>() {
			@Override
			public int compare(Person person1, Person person2) {
				return person1.getLastName().compareTo(person2.getLastName());
			};
		});
		System.out.println("=================================Printing all people=================================");
		printAllPeople(peoples);
		// Create a method that print all person with last name starting with S
		System.out.println(
				"=================================Printing all people whose last starts with S=================================");
		printPeopleByChar(peoples, new Condition() {

			@Override
			public boolean test(Person person) {
				return person.getLastName().startsWith("S");
			}
		});

		System.out.println(
				"=================================Printing all people whose First starts with S=================================");
		printPeopleByChar(peoples, new Condition() {

			@Override
			public boolean test(Person person) {
				return person.getFirstName().startsWith("S");
			}
		});
	}

	// Create a method that prints all elements in the list
	private static void printAllPeople(List<Person> peoples) {
		for (Person person : peoples) {
			System.out.println(person);
		}
	}

	private static void printPeopleByChar(List<Person> peoples, Condition condition) {
		for (Person person : peoples) {
			if (condition.test(person)) {
				System.out.println(person);
			}
		}
	}
}
