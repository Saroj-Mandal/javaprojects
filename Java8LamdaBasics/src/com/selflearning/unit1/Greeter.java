/**
 * 
 */
package com.selflearning.unit1;

import com.selflearning.common.Greeting;

/**
 * @author smand6
 *
 */
public class Greeter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Greeting greeting = () -> System.out.print("Hello World");
		Greeter greeter = new Greeter();
		greeter.greet(greeting);
	}

	public void greet(Greeting greeting) {
		greeting.perform();
	}

}
