package com.selflearning.unit1;

public class RunableTest {

	public static void main(String[] args) {
		Thread myThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				System.out.println("I am inside Runable");
			}
		});
		myThread.run();
		Runnable lamdaRunable = () -> System.out.println("We are inside lamda runable !");
		Thread myLamdaThread = new Thread(lamdaRunable);
		myLamdaThread.run();
	}
	
	

}
