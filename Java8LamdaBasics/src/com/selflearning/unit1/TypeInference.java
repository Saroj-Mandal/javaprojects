/**
 * 
 */
package com.selflearning.unit1;

/**
 * @author smand6
 *
 */
public class TypeInference {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TypeInferencelamda typeInferencelamda = (String s) -> s.length();
		TypeInferencelamda typeInferencelamda1 = (s) -> s.length();
		TypeInferencelamda typeInferencelamda2 = s -> s.length();
		
		printLength(typeInferencelamda, "Hello World");
		printLength(typeInferencelamda1, "Hello World 1");
		printLength(typeInferencelamda2, "Hello World 12");
	}
	
	public static void printLength(TypeInferencelamda typeInferencelamda, String s) {
		System.out.println(typeInferencelamda.getLength(s));
	}
	
	interface TypeInferencelamda {
		int getLength(String s);
	}

}
