package com.selflearning.unit1;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.selflearning.common.Condition;
import com.selflearning.common.Person;

public class Unit1ExceciseJava8Lamda {

	public static void main(String[] args) {
		// Create a list with person object
		List<Person> peoples = Arrays.asList(new Person("Saroj", "Mandal", 28),
				new Person("Vinothini", "Narayanasamy", 23), new Person("Sakshi", "Srivastava", 26),
				new Person("Hemanth", "Neelam", 25), new Person("Ashutodh", "Mahato", 31),
				new Person("Jayaprakash", "Ethiraja", 32), new Person("Rajasekhar", "Kendole", 32),
				new Person("Heena", "Srivastava", 28), new Person("Vijaya", "Kumar", 28));
		// Sort list by last name
		peoples.sort((Person person1, Person person2) -> person1.getLastName().compareTo(person2.getLastName()));
		Collections.sort(peoples,
				(Person person1, Person person2) -> person1.getLastName().compareTo(person2.getLastName()));
		System.out.println("=================================Printing all people=================================");
		printPeopleConditionaly(peoples, person -> true);
		System.out.println(
				"=================================Printing all people whose last starts with N=================================");
		performPeopleConditionalyUsingOOTB(peoples, person -> person.getLastName().startsWith("N"),
				person -> System.out.println(person.getFirstName() + " => " + person.getAge()));

		System.out.println(
				"=================================Printing all people whose First starts with H=================================");
		performPeopleConditionalyUsingOOTB(peoples, (person) -> person.getFirstName().startsWith("H"),
				person -> System.out.println(person));

		System.out.println(
				"=================================Printing all people whose First starts with A=================================");
		Condition fNameStartsWithA = (person) -> person.getFirstName().startsWith("A");
		printPeopleConditionaly(peoples, fNameStartsWithA);

		System.out.println(
				"=================================Printing all people whose First starts with K=================================");
		Condition lNameStartsWithK = (person) -> person.getLastName().startsWith("K");
		printPeopleConditionaly(peoples, lNameStartsWithK);
	}

	// Create a method that prints all elements conditionally
	private static void printPeopleConditionaly(List<Person> peoples, Condition condition) {
		for (Person person : peoples) {
			if (condition.test(person)) {
				System.out.println(person);
			}
		}
	}

	// Create a method that prints all elements conditionally
	private static void performPeopleConditionalyUsingOOTB(List<Person> peoples, Predicate<Person> predicate,
			Consumer<Person> consumer) {
		for (Person person : peoples) {
			if (predicate.test(person)) {
				consumer.accept(person);
			}
		}
	}
}
