package com.selflearning.unit1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.selflearning.common.Person;

public class Unit1Exercise {

	public static void main(String[] args) {
		// Create a list with person object
		List<Person> personList = Arrays.asList(new Person("Saroj", "Mandal", 28),
				new Person("Vinothini", "Narayanasamy", 23), new Person("Sakshi", "Srivastava", 26),
				new Person("Hemanth", "Neelam", 25), new Person("Ashutodh", "Mahato", 31),
				new Person("Jayaprakash", "Ethiraja", 32), new Person("Rajasekhar", "Kendole", 32),
				new Person("Heena", "Srivastava", 28), new Person("Vijaya", "Kumar", 28));
		// Sort list by last name
		personList.sort(new Comparator<Person>() {
			@Override
			public int compare(Person person1, Person person2) {
				return person1.getLastName().compareTo(person2.getLastName());
			};
		});
		// Create a method that prints all elements in the list
		printAllPerson(personList);
		System.out.println("================================================================================");
		// Create a method that print all person with last name starting with C
		printPersonByChar(personList, "S");
	}

	private static void printAllPerson(List<Person> personList) {
		for (Person person : personList) {
			System.out.println(person.toString());
		}
	}

	private static void printPersonByChar(List<Person> personList, String letter) {
		for (Person person : personList) {
			if (person.getLastName().startsWith(letter)) {
				System.out.println(person.toString());
			}
		}
	}
}
