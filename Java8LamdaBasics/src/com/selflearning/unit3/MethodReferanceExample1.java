/**
 * 
 */
package com.selflearning.unit3;

/**
 * @author smand6
 *
 */
public class MethodReferanceExample1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// () -> printMessage() === MethodReferanceExample1::printMessage
		Thread thread = new Thread(() -> printMessage());
		thread.start();
		
		//This is method referance
		Thread thread1 = new Thread(MethodReferanceExample1::printMessage);
		thread1.start();
	}

	private static void printMessage() {
		System.out.println("This is running in a thread...");
	}

}
