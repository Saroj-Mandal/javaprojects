package com.selflearning.unit3;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.selflearning.common.Person;

public class MethodReferanceExample2 {

	public static void main(String[] args) {
		// Create a list with person object
		List<Person> peoples = Arrays.asList(new Person("Saroj", "Mandal", 28),
				new Person("Vinothini", "Narayanasamy", 23), new Person("Sakshi", "Srivastava", 26),
				new Person("Hemanth", "Neelam", 25), new Person("Ashutodh", "Mahato", 31),
				new Person("Jayaprakash", "Ethiraja", 32), new Person("Rajasekhar", "Kendole", 32),
				new Person("Heena", "Srivastava", 28), new Person("Vijaya", "Kumar", 28));

		System.out.println(
				"=================================Printing all people whose First starts with H=================================");
		// person -> System.out.println(person) === System.out::println
		//Using method referance with single parameter
		performPeopleConditionalyUsingOOTB(peoples, (person) -> person.getFirstName().startsWith("H"),
				System.out::println);

	}

	// Create a method that prints all elements conditionally
	private static void performPeopleConditionalyUsingOOTB(List<Person> peoples, Predicate<Person> predicate,
			Consumer<Person> consumer) {
		for (Person person : peoples) {
			if (predicate.test(person)) {
				consumer.accept(person);
			}
		}
	}
}
