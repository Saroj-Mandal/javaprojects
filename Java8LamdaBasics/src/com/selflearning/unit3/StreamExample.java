package com.selflearning.unit3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.omg.Messaging.SyncScopeHelper;

import com.selflearning.common.Person;

public class StreamExample {
	public static void main(String[] args) {
		List<Person> peoples = Arrays.asList(new Person("Saroj", "Mandal", 28),
				new Person("Vinothini", "Narayanasamy", 23), new Person("Sakshi", "Srivastava", 26),
				new Person("Hemanth", "Neelam", 25), new Person("Ashutodh", "Mahato", 31),
				new Person("Jayaprakash", "Ethiraja", 32), new Person("Rajasekhar", "Kendole", 32),
				new Person("Heena", "Srivastava", 28), new Person("Vijaya", "Kumar", 28));

		peoples.stream().filter(p -> p.getLastName().startsWith("N"))
				.forEach(p -> System.out.println(p.getFirstName()));

		System.out.println("********** Executing more Stream programs **********");
		moreStreamExample();
	}

	private static void moreStreamExample() {
		// IntStream Example
		System.out.println("********** IntStream Example **********");
		IntStream.range(1, 10).forEach(System.out::print);
		System.out.println();

		// IntStream with skip Example
		System.out.println("********** IntStream with skip Example **********");
		IntStream.range(1, 10).skip(5).forEach(x -> System.out.println(x));

		// IntStream with sum Example
		System.out.println("********** IntStream with sum Example **********");
		System.out.println(IntStream.range(1, 10).sum());

		// Stream.of with sorted and find first Example
		System.out.println("********** Stream.of with sorted and find first Example **********");
		Stream.of("Saroj", "Vini", "Sakshi").sorted().findFirst().ifPresent(System.out::print);
		System.out.println();

		// Stream from array then sort, filter and print Example
		System.out.println("********** Stream from array then sort, filter and print Example **********");
		String[] array = { "Saroj", "Vini", "Sakshi", "Ashutosh", "Hemanth", "Heena", "Vijay", "Shweta", "rajasekhar" };
		Arrays.stream(array) // same as Stream.of(array);
				.filter(x -> x.startsWith("S")).sorted().forEach(System.out::println);

		// Average of squares of an int array Example
		System.out.println("********** Average of squares of an int array Example  **********");
		int[] intArray = { 2, 4, 6, 8, 10 };
		Arrays.stream(intArray) // same as Stream.of(array);
				.map(x -> x * x).average().ifPresent(System.out::println);
		System.out.println();

		// Stream from list then filter and print Example
		System.out.println("********** Stream from list then filter and print Example  **********");
		List<String> people = Arrays.asList(array);
		people.stream().map((x) -> x.toLowerCase()).filter(x -> x.startsWith("h")).forEach(System.out::println);
		System.out.println();

		// Stream rows from text file then sort, filter and print Example
		System.out.println("********** Stream rows from text file then sort, filter and print Example  **********");
		try {
			Stream<String> bands = Files
					.lines(Paths.get("D:\\SelfLearning\\workspace\\Java8LamdaBasics\\flat-files\\band.txt"));
			bands.sorted().filter(x -> x.length() < 15).forEach(System.out::println);
			bands.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println();

		// Stream rows from text file then save it to list Example
		System.out.println("********** Stream rows from text file then save it to list Example  **********");
		try {
			List<String> bands = Files
					.lines(Paths.get("D:\\SelfLearning\\workspace\\Java8LamdaBasics\\flat-files\\band.txt")).sorted()
					.filter(x -> x.contains("00")).collect(Collectors.toList());
			bands.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println();

		// Stream rows from csv file then count valid rows Example
		System.out.println("********** Stream rows from csv file then count valid rows Example  **********");
		try {
			Stream<String> rows = Files
					.lines(Paths.get("D:\\SelfLearning\\workspace\\Java8LamdaBasics\\flat-files\\data.csv"));
			long count = rows.filter(x -> x.split(",").length == 3).count();
			System.out.println("Valid Row count is :" + count);
			rows.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println();

		// Stream rows from csv file then filter, parse rows Example
		System.out.println("********** Stream rows from csv file then filter, parse rows Example  **********");
		try {
			Stream<String> rows = Files
					.lines(Paths.get("D:\\SelfLearning\\workspace\\Java8LamdaBasics\\flat-files\\data.csv"));
			rows.map(x -> x.split(",")).filter(x -> x.length == 3)
					.filter(x -> Integer.parseInt(x[1]) > 17 || Integer.parseInt(x[1]) < 13)
					.forEach(x -> System.out.println(x[0] + " : " + x[1] + " : " + x[2]));
			rows.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println();

		// Stream rows from csv file then store fields in HashMap Example
		System.out.println("********** Stream rows from csv file then store fields in HashMap Example  **********");
		try {
			Stream<String> rows = Files
					.lines(Paths.get("D:\\SelfLearning\\workspace\\Java8LamdaBasics\\flat-files\\data.csv"));
			Map<String, Integer> newHashMap = new HashMap<>();
			newHashMap = rows.map(x -> x.split(",")).filter(x -> x.length == 3)
					.filter(x -> Integer.parseInt(x[1]) > 17 || Integer.parseInt(x[1]) < 13)
					.collect(Collectors.toMap(x -> x[0], x -> Integer.parseInt(x[1])));
			rows.close();
			for (String key : newHashMap.keySet()) {
				System.out.println(key + " : " + newHashMap.get(key));
			}
			Stream.of(newHashMap).forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println();

		// Reduction sum Example
		System.out.println("********** Reduction sum Example  **********");
		double total = Stream.of(10.3, 10.2, 10.5).reduce(0.0, (Double a, Double b) -> a + b);
		System.out.println("Total is : " + total);

		// Reduction summary statistics Example
		System.out.println("********** Reduction summary statistics  **********");
		IntSummaryStatistics statistics = IntStream.of(2, 4, 5, 7, 3, 9, 29, 20, 34, 50).summaryStatistics();
		System.out.println("Summary Statistics : " + statistics);
	}
}
