/**
 * 
 */
package com.selflearning.unit3;

import java.util.Arrays;
import java.util.List;

import com.selflearning.common.Person;

/**
 * @author smand6
 *
 */
public class CollectionIterationExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<Person> peoples = Arrays.asList(new Person("Saroj", "Mandal", 28),
				new Person("Vinothini", "Narayanasamy", 23), new Person("Sakshi", "Srivastava", 26),
				new Person("Hemanth", "Neelam", 25), new Person("Ashutodh", "Mahato", 31),
				new Person("Jayaprakash", "Ethiraja", 32), new Person("Rajasekhar", "Kendole", 32),
				new Person("Heena", "Srivastava", 28), new Person("Vijaya", "Kumar", 28));

		// Using for each loop
		System.out.println("**********Using conventional for loop**********");
		for (int count = 0; count < peoples.size(); count++) {
			System.out.println(peoples.get(count));
		}

		// Using pre-Java8 for each loop
		System.out.println("**********Using pre-java8 foreach loop**********");
		for (Person person : peoples) {
			System.out.println(person);
		}

		// Using java8 foreach loop and lamda
		System.out.println("**********Using post-java8 foreach loop and lamda**********");
		peoples.forEach(p -> System.out.println(p));

		// Using java8 foreach loop and method referance
		System.out.println("**********Using post-java8 foreach loop and method referance**********");
		peoples.forEach(System.out::println);
	}

}
