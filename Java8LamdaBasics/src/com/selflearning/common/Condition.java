package com.selflearning.common;

public interface Condition {
	public boolean test(Person person);
}
