/**
 * 
 */
package com.selflearning.common;

/**
 * @author smand6
 *
 */

@FunctionalInterface
public interface Greeting {
	public void perform();
}
