package com.selflearning.unit2;

import com.selflearning.common.Process;

public class ClosureExample {
	public static void main(String[] args) {
		int i = 10;
		int b = 20;
		doProcess(i, p -> System.out.println(i + b));
	}

	private static void doProcess(int i, Process p) {
		p.process(i);
	}
}
