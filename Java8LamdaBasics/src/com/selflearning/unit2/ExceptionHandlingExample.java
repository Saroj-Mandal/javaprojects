/**
 * 
 */
package com.selflearning.unit2;

import java.util.function.BiConsumer;

/**
 * @author smand6
 *
 */
public class ExceptionHandlingExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] someNumbers = { 1, 2, 3, 4, 5 };
		int key = 0;
		process(someNumbers, key, wrapperLamda((num, tempKey) -> System.out.println(num / tempKey)));
	}

	private static void process(int[] someNumbers, int key, BiConsumer<Integer, Integer> biConsumer) {
		for (int num : someNumbers) {
			biConsumer.accept(num, key);
		}
	}

	private static BiConsumer<Integer, Integer> wrapperLamda(BiConsumer<Integer, Integer> biConsumer) {
		return (num, tempKey) -> {
			try {
				biConsumer.accept(num, tempKey);
			} catch (Exception e) {
				System.out.println("Exception Handled " + e.getMessage());
			}
		};
	}

}
