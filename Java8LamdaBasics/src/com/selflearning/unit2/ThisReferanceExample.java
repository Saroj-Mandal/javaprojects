package com.selflearning.unit2;

import com.selflearning.common.Process;

public class ThisReferanceExample {
	public static void main(String[] args) {
		// Using anonymous inner class
		int i = 10;
		doProcess(i, new Process() {

			@Override
			public void process(int i) {
				System.out.println("Using anonymous inner class " + i);
				System.out.println(this);
			}

			@Override
			public String toString() {
				return "This is the anonymous inner class !...";
			}
		});

		// Using lamda to test this referance
		doProcess(i, p -> System.out.println("Using Lamda " + i));

		ThisReferanceExample thisReferanceExample = new ThisReferanceExample();
		thisReferanceExample.doProcessUsingLamda(i);

	}

	private static void doProcess(int i, Process p) {
		p.process(i);
	}

	private void doProcessUsingLamda(int i) {
		// Here this referring to the class ThisReferanceExample inside lamda.
		doProcess(i, p -> {
			System.out.println("Using Lamda through a private method " + i);
			System.out.println(this);
		});
	}

	@Override
	public String toString() {
		return "This is the ThisReferanceExample class !***";
	}

}
